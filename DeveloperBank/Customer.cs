﻿public class Customer
{
    public string Name { get; set; }
    public decimal WithdrawalAmount { get; set; }
    public int OrderNumber { get; set; }
}
